require = require('@std/esm')(module, {esm: 'mjs', cjs: true});

const OpenXum = require('../../../lib/openxum-core/').default;
const AI = require('../../../lib/openxum-ai/').default;

let e = new OpenXum.Zertz.Engine(OpenXum.Zertz.GameType.REGULAR, OpenXum.Zertz.Color.ONE);
let p1 = new AI.Generic.RandomPlayer(OpenXum.Zertz.Color.ONE, OpenXum.Zertz.Color.TWO, e);
let p2 = new AI.Generic.RandomPlayer(OpenXum.Zertz.Color.TWO, OpenXum.Zertz.Color.ONE, e);
let p = p1;
let moves = [];

while (!e.is_finished()) {
  let move = p.move();

  if (move.constructor === Array) {
    for (let i = 0; i < move.length; ++i) {
      console.log(move[i].to_string());
    }
  } else {
    console.log(move.to_string());
  }

  moves.push(move);
  e.move(move);
  p = p === p1 ? p2 : p1;
}

console.log("Winner is " + (e.winner_is() === OpenXum.Zertz.Color.ONE ? "one" : "two"));
for (let index = 0; index < moves.length; ++index) {
  console.log(moves[index].to_string());
}