require = require('@std/esm')(module, {esm: 'mjs', cjs: true});

const OpenXum = require('../../../lib/openxum-core/').default;
const AI = require('../../../lib/openxum-ai/').default;

let e = new OpenXum.XXX.Engine(OpenXum.P_9TKA.GameType.STANDARD, OpenXum.P_9TKA.Color.RED);
let p1 = new AI.Generic.RandomPlayer(OpenXum.P_9TKA.Color.BLACK, OpenXum.P_9TKA.Color.WHITE, e);
let p2 = new AI.Generic.RandomPlayer(OpenXum.P_9TKA.Color.WHITE, OpenXum.P_9TKA.Color.BLACK, e);
let p = p1;
let moves = [];

while (!e.is_finished()) {
    const move = p.move();

    moves.push(move);
    e.move(move);
    p = p === p1 ? p2 : p1;
}

console.log("Winner is " + (e.winner_is() === OpenXum.P_9TKA.Color.WHITE ? "aaa" : "bbb"));
for (let index = 0; index < moves.length; ++index) {
    console.log(moves[index].to_string());
}
