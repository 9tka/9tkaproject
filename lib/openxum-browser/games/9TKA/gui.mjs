"use strict";


import C_9Tka from '../../../openxum-core/games/9TKA/index.mjs';
import Graphics from '../../graphics/index.mjs';
import OpenXum from '../../openxum/gui.mjs';

const colors = [
    ['orange', 'blue', 'purple', 'pink', 'yellow', 'red', 'green', 'brown'],
    ['red', 'orange', 'pink', 'green', 'blue', 'yellow', 'brown', 'purple'],
    ['green', 'pink', 'orange', 'red', 'purple', 'brown', 'yellow', 'blue'],
    ['pink', 'purple', 'blue', 'orange', 'brown', 'green', 'red', 'yellow'],
    ['yellow', 'red', 'green', 'brown', 'orange', 'blue', 'purple', 'pink'],
    ['blue', 'yellow', 'brown', 'purple', 'red', 'orange', 'pink', 'green'],
    ['purple', 'brown', 'yellow', 'blue', 'green', 'pink', 'orange', 'red'],
    ['brown', 'green', 'red', 'yellow', 'pink', 'purple', 'blue', 'orange']
];

const greenCords = [[1, 1, 4, 1, 4, 4, 1, 4, 1, 1],
    [7, 1, 10, 1, 10, 4, 7, 4, 7, 1],
    [7, 7, 10, 7, 10, 10, 7, 10, 7, 7],
    [4, 4, 7, 4, 7, 7, 4, 7, 4, 4],
    [1, 7, 4, 7, 4, 10, 1, 10, 1, 10]
];


// ...

class Gui extends OpenXum.Gui {
    constructor(c, e, l, g) {
        super(c, e, l, g);

        // Vos attributs...



    }

    draw() {
        // La méthode principale de la classe qui se charge de dessiner à l'écran
        // (le plateau, les pièces, les mouvements possibles, ...)

        // fond
        this._context.clearRect(0, 0, this._canvas.width, this._canvas.height);
        this._context.lineWidth = 10;
        this._context.strokeStyle = "#ffffff";
        this._context.fillStyle = "#000000";

        // background
        this._context.strokeStyle = "#ffffff";
        this._context.fillStyle = "#ffffff";
        Graphics.board.draw_round_rect(this._context, 0, 0, this._canvas.width, this._canvas.height, 17, true, true);
        this._draw_grid();

        // this._engine


        // implémenter le reste du rendu ici
    }

    get_move() {
        // Retourne le mouvement à effectuer par le manager pour le tour actuel
        // Un objet de type Move doit être construit ; si ce n'est pas le cas,
        // alors la méthode process_move sera invoquée
    }

    is_animate() {
        // Retourne true si le coup provoque des animations
        // (déplacement de pions, par exemple).
        return false;
    }

    is_remote() {
        // Indique si un joueur joue à distance
        return false;
    }

    move(move, color) {
        this._manager.play();
        // TODO !!!!!
    }

    unselect() {
        // Remet à zéro tous les attributs relatifs au coup qui vient d'être joué
    }

    set_canvas(c) {
        super.set_canvas(c);


        this._height = this._canvas.height;
        this._width = this._canvas.width;

        this._deltaX = (this._canvas.width) / 11;
        this._deltaY = (this._canvas.height) / 11;
        this._offsetX = this._canvas.width / 2 - this._deltaX * 5.5;
        this._offsetY = this._canvas.height / 2 - this._deltaY * 5.5;


        this._canvas.addEventListener("click", (e) => {
            this._on_click(e);
        });

        this.draw(); // Ne pas oublier de dessiner le plateau une première fois !

       // console.log(this._engine._get_obstacles_list());

        this._engine._put_obstacles();
        const obstacles_list = this._engine._get_obstacles_list();
        for (let i = 0; i < obstacles_list.length; i++) {
            this._draw_obstacle(obstacles_list [i].coordinates());
        }
    }


    _get_click_position(e) {
        let rect = this._canvas.getBoundingClientRect();
        let x = (e.clientX - rect.left) * this._scaleX - this._offsetX;
        let y = (e.clientY - rect.top) * this._scaleY - this._offsetY;

        return {x: Math.floor(x / this._deltaX), y: Math.floor(y / this._deltaX)};
    }

    _to_pixel_coords(x, y) {

        return [this._offsetX + x * this._deltaX + (this._deltaX / 2) - 1, this._offsetY + y * this._deltaY + (this._deltaY / 2) - 1];

    }


    _on_click(event) {

        let index_coords = this._get_click_position(event);

        let pixel_coords = this._to_pixel_coords(index_coords.x, index_coords.y);



        let unselected_piece =  this._engine._unselect_piece();

        //console.log(unselected_piece);

        if (unselected_piece !== undefined && this._engine._is_full()) {

        let index_coords_unselected = unselected_piece.coordinates() ;

        let pixel_coords_unselected = this._to_pixel_coords(index_coords_unselected.x(), index_coords_unselected.y());

        this._draw_unselect_circle(pixel_coords_unselected,index_coords_unselected) ;

        //console.log ("drawing white circle ");
        }



        //const slots = this._placement_slots();

        //const clickpos = this._get_click_position(event);

        //let piece = new Piece(C_9Tka.Color.YELLOW, new Coordinates(index_coords.x, index_coords.y));


        // console.log(this._engine.init_board._is_placement_coords(index_coords), this._engine.init_board._slot_taken(index_coords), this._engine.get_phase(), this._engine._is_full());

        //console.log (" ISS IIIIIIIIIT ?", this._engine.is_finished());
        if (this._engine.init_board._is_placement_coords(index_coords) && (!this._engine.init_board._slot_taken(index_coords))
            && (this._engine.get_phase() === 0) && (!this._engine._is_full())) {
            // TODO :: change color after each placement until the board is full then change phase !!
            /*
            let piece = new Piece(C_9Tka.Color.YELLOW, new Coordinates(index_coords.x, index_coords.y));
            this._engine.init_board._fill_slot(piece);*/

            this._engine._put_piece(index_coords);
            this._draw_cap(this._engine._get_piece_At(index_coords));
            this._engine._change_color();
            //console.log(this._engine.get_phase()) ;


        }







        else if (this._engine.init_board._is_placement_coords(index_coords) && this._engine.init_board._slot_taken(index_coords)
                && (this._engine.get_phase() === 1) && !this._engine.is_finished()) {

            console.log ("FINISHED ?",this._engine.is_finished()) ;

        /*else if (this._engine.init_board._is_placement_coords(index_coords) && this._engine.init_board._slot_taken(index_coords) &&
                !this._engine.init_board._get_piece_At(index_coords).is_selected() && this._engine.get_phase() === 1) {*/



            // TODO :: implement selection function when u done with placement phase !!

           // TODO :: get_selected_piece --> unselect it ;






          //  this._engine._select_piece(index_coords);

            //this._draw_select_circle(pixel_coords, index_coords);

            //console.log(" piece selected i must draw o grey circle");

            // MOVING ---

            //console.log(this._engine.move(index_coords));


            //this._engine.move(index_coords);


            this._draw_cap(this._engine.move(index_coords));

            this._hide_cap (pixel_coords);

            this._engine._change_color();

            console.log( "Current COLOR",this._engine.current_color());

           //console.log ("IS FINISHED ? " , this._engine.is_finished());















        }











        else if (this._engine._is_full()){

            //console.log(this._engine.init_board._is_placement_coords(index_coords) , this._engine.init_board._slot_taken(index_coords),this._engine.init_board._get_piece_At(index_coords).is_selected(),this._engine.get_phase());
            //console.log (" BOARD FULL CHANGING NOW YOU CAN PLAY!!! ") ;

            this._engine.change_phase();

           // console.log ("FINISH ?",this._engine.is_finished());





            //console.log("CHEKING ---")
            //console.log(this.old_index_coords);
            //console.log(this._engine.init_board._get_piece_At(this.old_index_coords))
            //console.log(this._engine.init_board._get_piece_At(this.old_index_coords))

            //console.log(" i must draw");




        }

        else if (this._engine.is_finished())    {

            console.log("FINISHED ?" , this._engine.is_finished()) ;
            this._engine._change_color();
            this._engine.winner_is();
        }




    }


    _hide_cap (pixel_coords) {

        this._context.fillStyle = "#FFFFFF" ;
        this._context.beginPath();
        this._context.arc(pixel_coords[0], pixel_coords[1], 23, 0, 2 * Math.PI);
        this._context.closePath();
        this._context.fill();
       // console.log ("HIDING OLD PIECE" ) ;
    }



    _draw_select_circle(pixel_coords, index_coords) {

        if (this._engine.init_board._get_piece_At(index_coords).is_selected()) this._context.strokeStyle = "#C0C0C0";
        else this._context.strokeStyle = "#D3D3D3";
        this._context.lineWidth = 2;

        this._context.beginPath();
        this._context.arc(pixel_coords[0], pixel_coords[1], 22, 0, 2 * Math.PI);
        this._context.closePath();

        this._context.stroke();


        // console.log("ok");


    }

    _draw_unselect_circle(pixel_coords, index_coords)  {

        this._context.strokeStyle = "#FFFFFF";
        this._context.lineWidth = 2;

        this._context.beginPath();
        this._context.arc(pixel_coords[0], pixel_coords[1], 22, 0, 2 * Math.PI);
        this._context.closePath();

        this._context.stroke();


        // console.log("ok");


    }



    _placement_slots() {


        let _valid_slots = [];

        // top slots

        for (let i = 1; i < 9; i++) {

            _valid_slots.push([(this._width / 11) * i, this._height / 11]);
        }
        // right slots

        for (let i = 1; i < 9; i++) {

            _valid_slots.push([(this._width / 11) * 10, (this._height / 11) * i]);
        }

        // bot slots

        for (let i = 1; i < 9; i++) {

            _valid_slots.push([(this._width / 11) * i, (this._height / 11) * 10]);
        }

        // left slots

        for (let i = 1; i < 9; i++) {

            _valid_slots.push([0, (this._height / 11) * i]);
        }


        return _valid_slots;

    }

    _draw_obstacle(position) {


        /*

        let min =  1;
        let max = 4;

        let random_position_coords = {
            x: Math.floor((Math.random() * (max - min)) + min ) ,
            y: Math.floor((Math.random()* (max - min)) + min )
        };

        let pixel_coords = this._to_pixel_coords(random_position_coords.x,random_position_coords.y);


         */

        let piece_pixel_coords = this._to_pixel_coords(position.x(), position.y());

        this._context.fillStyle = '#FF0000';
        this._context.beginPath();
        this._context.arc(piece_pixel_coords[0], piece_pixel_coords[1], 20, 0, 2 * Math.PI);
        this._context.fill();
        this._context.stroke();


    }


    _draw_cap(piece) {

        let piece_pixel_coords = this._to_pixel_coords(piece.coordinates().x(), piece.coordinates().y());
        if (piece.color() === C_9Tka.Color.BLUE)
            this._context.fillStyle = '#1E90FF';
        else this._context.fillStyle = '#FFFF00';


        //console.log(piece_pixel_coords[0]);
        //console.log(piece_pixel_coords[1]);


        this._context.beginPath();
        this._context.arc(piece_pixel_coords[0], piece_pixel_coords[1], 20, 0, 2 * Math.PI);
        this._context.fill();
        //this._context.stroke();
        this._context.closePath();


        this._context.strokeStyle = '#FFFFFF';
        this._context.lineWidth = 2;

        this._context.beginPath();
        this._context.arc(piece_pixel_coords[0], piece_pixel_coords[1], 22, 0, 2 * Math.PI);
        this._context.closePath();

        this._context.stroke();


    }


    _draw_grid() {


        this._context.strokeStyle = "#000000";
        this._context.lineWidth = 3;

        this._context.beginPath();
        this._context.moveTo((this._width / 11), (this._height / 11));
        this._context.lineTo((this._width / 11) * 10, this._height / 11);
        this._context.lineTo((this._width / 11) * 10, (this._height / 11) * 10);
        this._context.lineTo((this._width / 11), (this._height / 11) * 10);
        this._context.lineTo((this._width / 11), (this._height / 11));
        this._context.closePath();

        this._context.fillStyle = "#ffff99";
        this._context.fill();
        this._context.stroke();


        this._context.fillStyle = "#99ff66";

        for (let i = 0; i < greenCords.length; i++) {

            this._context.beginPath();
            this._context.moveTo((this._width / 11) * greenCords[i][0], (this._height / 11) * greenCords[i][1]);
            this._context.lineTo((this._width / 11) * greenCords[i][2], (this._height / 11) * greenCords[i][3]);
            this._context.lineTo((this._width / 11) * greenCords[i][4], (this._height / 11) * greenCords[i][5]);
            this._context.lineTo((this._width / 11) * greenCords[i][6], (this._height / 11) * greenCords[i][7]);
            this._context.lineTo((this._width / 11) * greenCords[i][8], (this._height / 11) * greenCords[i][9]);
            this._context.closePath();
            this._context.fill();
            this._context.stroke();


        }


        this._context.lineWidth = 1;


        for (let i = 1; i < 11; i++) {


            this._context.fillStyle = colors[0][0];


            this._context.beginPath();
            this._context.moveTo((this._width / 11) * i, 0);
            this._context.lineTo((this._width / 11) * i, this._height);
            this._context.stroke();
            //this._context.closePath() ;

            this._context.beginPath();
            this._context.moveTo(0, (this._height / 11) * i);
            this._context.lineTo(this._width, (this._height / 11) * i);
            this._context.fill();
            this._context.stroke();
            //this._context.closePath() ;

        }


    }


}

export default Gui;
