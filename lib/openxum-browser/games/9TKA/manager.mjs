"use strict";


import OpenXum from '../../openxum/manager.mjs';
import P_9Tka from '../../../openxum-core/games/9TKA/index.mjs';


class Manager extends OpenXum.Manager {
    constructor(e, g, o, s) {
        super(e, g, o, s);
        // ...
    }

    build_move() {
        return new P_9Tka.Move();
    }

    get_current_color() {
        return this._engine.current_color() === Color.YELLOW ? 'Yellow' : 'Blue';
    }

    get_name() {
        return '9TKA';
    }

    get_winner_color() {
        return this.engine().winner_is() === Color.WHITE ? 'yellow' : 'blue';
    }

    process_move() {  }
}

export default Manager;
