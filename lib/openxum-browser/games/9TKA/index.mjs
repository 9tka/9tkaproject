"use strict";

import P_9Tka from '../../../openxum-core/games/9TKA/index.mjs';
import AI from '../../../openxum-ai/generic/index.mjs';
import Gui from './gui.mjs';
import Manager from './manager.mjs';


export default {
    Gui: Gui,
    Manager: Manager,
    Settings: {
        ai: {
            mcts: AI.RandomPlayer
        },
        colors: {
            first: P_9Tka.Color.YELLOW,
            init: P_9Tka.Color.YELLOW,
            list: [
                {key: P_9Tka.Color.YELLOW, value: 'yellow'},
                {key: P_9Tka.Color.BLUE, value: 'blue'}
            ]
        },
        modes: {
            init: P_9Tka.GameType.STANDARD,
            list: [
                {key: P_9Tka.GameType.STANDARD, value: 'standard'}
            ]
        },
        opponent_color(color) {
            return color === P_9Tka.Color.YELLOW ? P_9Tka.Color.BLUE : P_9Tka.Color.YELLOW;
        },
        types: {
            init: 'ai',
            list: [
                {key: 'gui', value: 'GUI'},
                {key: 'ai', value: 'AI'},
                {key: 'online', value: 'Online'},
                {key: 'offline', value: 'Offline'}
            ]
        }
    }
};
