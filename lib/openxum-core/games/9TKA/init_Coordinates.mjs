"use strict";

class init_Coordinates {

    constructor(c, l) {
        this._column = c;
        this._line = l;
    }


    isValid() {

        return (this._column == 0  && this._line >= 1 && this._line<=10 ) ||
            (this._column >= 0 && this._column<=10 && this._line == 0 ) ||
            (this._column == 10  && this._line >= 0 && this._line <=10 ) ||
            (this._column <=10 && this._column>=0  && this._line == 10 )
            ;
    }



    clone() {
        return new init_Coordinates(this._column, this._line);
    };



    x() {
        return this._column;
    };


    y() {
        return this._line;
    };


    hash() {

        return this._column.charCodeAt(0) + this._line * (4 * this._line) - 7;
    };


    equals(init_Coordinates) {
        return this._column === init_Coordinates.x() &&
            this._line === init_Coordinates.y();
    };


    to_string() {
        if (!this.isValid()) {
            return "invalid";
        }
        return this._column + "" + this._line;
    }


    formatted_string() {
        return String.fromCharCode('A'.charCodeAt(0) + this.x()) + (this.y() + 1);
    }
}

export default init_Coordinates;