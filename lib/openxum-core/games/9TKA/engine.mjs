// lib/openxum-core/games/newgame/engine.mjs

//import OpenXum from '../../openxum/index.mjs';
/*
import Color from './color.mjs';
import Coordinates from './coordinates.mjs';
import init_Coordinates from './coordinates.mjs';
//import Intersection from './intersection.mjs';
*/

import Move from './move.mjs';
import OpenXum from '../../openxum/index.mjs';
import Color from './color.mjs';
import Dimension from './dimension.mjs';
import Board from './board.mjs';
import Phase from './phase.mjs' ;
import Piece from "./piece.mjs";
import C_9Tka from "./index.mjs";
import Coordinates from "./coordinates.mjs";
//import Phase from './move_type.mjs';
//import les autres classes dont vous avez besoin

class Engine extends OpenXum.Engine {

    constructor(t, c) {

        super();
        this._type = t;
        this._current_color = c;
        this._first_color = c;


        // autres attributs nécessaires à votre jeu
        this._opponent_color = (c === Color.YELLOW) ? Color.BLUE : Color.YELLOW;
        this._piece_clone = undefined;
        console.log("INIT", this._opponent_color);

        this._winner_color = Color.NONE;
        this._finished = false;

        this._phase = Phase.PLACEMENT;

        this._yPieceNbr = 18;
        this._bPieceNbr = 18;


        this.init_board = new Board(Dimension.DEFAULT_COLUMNS, Dimension.DEFAULT_LINES);

        // TODO : LISTE DES COORDONNÉE !!! ==> classe coordonnées


    }

    _put_piece(index_coords) {

        console.log("PUT", this._current_color);
        if (this._phase === Phase.PLACEMENT) {
            if (this._current_color === 1) {
                let piece = new Piece(C_9Tka.Color.YELLOW, new Coordinates(index_coords.x, index_coords.y));
                this.init_board._fill_slot(piece);

            } else if (this._current_color === 2) {

                let piece = new Piece(C_9Tka.Color.BLUE, new Coordinates(index_coords.x, index_coords.y));
                this.init_board._fill_slot(piece);

            }

        } else {

            this.init_board._fill_slot(this._piece_clone);


        }


    }


    _put_obstacles() {

        let i = 9;

        let min_x = 1;
        let max_x = 4;

        let min_y = 1;
        let max_y = 4;

        let random_position_coords = undefined;

        while (i != 0) {

            random_position_coords = {
                x: Math.floor((Math.random() * (max_x - min_x)) + min_x),
                y: Math.floor((Math.random() * (max_y - min_y)) + min_y)
            };

            let obstacle = new Piece(C_9Tka.Color.RED, new Coordinates(random_position_coords.x, random_position_coords.y));
            this.init_board._fill_slot(obstacle);

            min_x = max_x;
            max_x += 3;

            if (max_x > 10) {

                min_x = 1;
                max_x = 4;

                min_y = max_y;
                max_y += 3;

            }

            i--;
        }

    }

    _select_piece(position) {

        if (this.get_phase() === Phase.MOVEMENT && (!this._get_piece_At(position).is_selected()) &&
            (this._get_piece_At(position).color() === Color.YELLOW
                || this._get_piece_At(position).color() === Color.BLUE))

            this._get_piece_At(position).select_piece();

        //console.log("SELECTED",this._get_piece_At(position).is_selected());

    }


    _unselect_piece() {

        return this.init_board._unselect_anything();

        //console.log("SELECTED",this._get_piece_At(position).is_selected());

    }


    _get_obstacles_list() {
        return this.init_board._get_obstacles();
    }

    _get_piece_At(position) {

        return this.init_board._get_piece_At(position);
    }


    _is_full() {

        return this.init_board._is_full();
    }


    build_move(c_position, n_position) {

        return new Move(position);
    }


    clone() {
        let e = new Engine(this._type, this._current_color, this._first_color);
        e._winner_color = this._winner_color;
        e._finished = this._finished;
        e._current_color = this._current_color;


        return e;

    }

    current_color() {
        return this._current_color;
    }

    get_name() {
        return '9TKA';
    }

    get_possible_move_list() {
        let moves = [];
        let index = this._get_pieces_player_index();
        for (let i = 0; i < this.board_init[index].length; i++) {


        }
        // console.log(moves.length + ' coups possibles pour ' + this.current_color());
        return moves;


    }

    is_finished() {


        return this.init_board.check_finish(this.current_color());


    }


    move(position) {

        console.log("IS FINISHED ,,,???? ------ > " , this.is_finished());
        console.log(this._current_color);
        if (this.init_board._is_placement_coords(position) && (this._get_piece_At(position) !== undefined )&& (this._phase === Phase.MOVEMENT)
            && (this._get_piece_At(position).color() === this._current_color ) && (!this.is_finished())){
            console.log(" I MUST MOVE");

        //&& (!this.is_finished()

            return this.get_moved_piece(position);
        }

        else if (this.is_finished()) this._change_color() ;
        /*
        else {
            this._change_color()
            return this.get_moved_piece(position);
        }*/
    }

    get_moved_piece(old_position) {


        // to bot
        console.log(old_position);


        if (old_position.y === 0) {

            for (let i = 1; i < 11; i++) {
                let new_position = {
                    x: old_position.x,
                    y: i
                }
                if (this.init_board._get_piece_At(new_position) !== undefined && old_position!==new_position) {
                    new_position.y -= 1;

                    this._piece_clone = this._get_piece_At(old_position).clone();
                    console.log("MOVE FROM TO", old_position, new_position);
                    this._piece_clone.set_coordinates(new Coordinates(new_position.x, new_position.y));
                    this._put_piece(new_position);
                    this.init_board._pop_piece(old_position);
                    return this._get_piece_At(new_position);
                }
            }

        }
        // to left
        if (old_position.x === 10) {
            console.log("YOOOOOOOOO");

            for (let i = 9; i > -1; i--) {
                //  console.log(j);
                let new_position = {
                    x: i,
                    y: old_position.y
                }

                    if (this.init_board._get_piece_At(new_position) !== undefined && old_position !==new_position) {
                        new_position.x += 1;


                        this._piece_clone = this._get_piece_At(old_position).clone();
                        console.log("MOVE FROM , TO", old_position, new_position);
                        this._piece_clone.set_coordinates(new Coordinates(new_position.x, new_position.y));
                        this._piece_clone.play();
                        this._put_piece(new_position);
                        this.init_board._pop_piece(old_position);
                        return this._get_piece_At(new_position);
                    }
            }
        }

        // to top

        if (old_position.y === 10) {
            console.log("MOVE", old_position);

            for (let i = 9; i > -1; i--) {
                let new_position = {
                    x: old_position.x,
                    y: i
                }
                if (this.init_board._get_piece_At(new_position) !== undefined && old_position !==new_position) {
                    new_position.y += 1;
                    console.log("MOVE FROM TO", old_position, new_position);
                    this._piece_clone = this._get_piece_At(old_position).clone();
                    this._piece_clone.set_coordinates(new Coordinates(new_position.x, new_position.y));
                    this._piece_clone.play();
                    this._put_piece(new_position);
                    this.init_board._pop_piece(old_position);
                    return this._get_piece_At(new_position);
                }
            }
        }


        // to right

        if (old_position.x === 0) {
            console.log("MOVE", old_position);

            for (let i = 1; i < 11; i++) {
                let new_position = {
                    x: i,
                    y: old_position.y
                }

                if (this.init_board._get_piece_At(new_position) !== undefined && old_position !== new_position) {
                    new_position.x -= 1;
                    console.log("MOVE FROM TO", old_position, new_position);
                    this._piece_clone = this._get_piece_At(old_position).clone();
                    this._piece_clone.set_coordinates(new Coordinates(new_position.x, new_position.y));
                    this._piece_clone.play();
                    this._put_piece(new_position);
                    this.init_board._pop_piece(old_position);
                    return this._get_piece_At(new_position);
                }
            }
        }
    }


    parse(str) {
        // TODO
    }

    to_string() {
        // TODO
    }

    winner_is() {
      this.init_board.check_dominant()  ;
    }



    is_fortress(x, y) {
        return x == 0 && y == 0 || x == 0 && y == 10 || x == 10 && y == 0 || x == 10 && y == 10 || x == 5 && y == 5;
    }


    _change_color() {
        this._current_color = (this._current_color === this._first_color) ? this._opponent_color : this._first_color;
    }

    get_current_color() {

        return this._current_color;
    }

    change_phase() {

        if (this._phase === Phase.PLACEMENT) this._phase = Phase.MOVEMENT;

    }

    get_phase() {
        return this._phase;
    }
}


export default Engine
