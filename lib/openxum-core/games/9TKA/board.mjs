"use strict";

import Color from './color.mjs';
//import Coordinates from "./coordinates";

class Board {

    constructor(columns, lines) {

        this.columns = columns;
        this.lines = lines;

        this.count_played_pieces = 0 ;

        this._blue_finished = false ;
        this._yellow_finished = false ;

        this._yellow_score = {
            area_0 : 0,
            area_1 : 0,
            area_2 : 0,
            area_3 : 0,
            area_4 : 0,
            area_5 : 0,
            area_6 : 0,
            area_7 : 0,
            area_8 : 0
            };

        this._blue_score = {
            area_0 : 0,
            area_1 : 0,
            area_2 : 0,
            area_3 : 0,
            area_4 : 0,
            area_5 : 0,
            area_6 : 0,
            area_7 : 0,
            area_8 : 0
        };

        this._board = new Array(lines);

        for (let i = 0; i < columns; i++) {
            this._board[i] = new Array(lines);
            for (let j = 0; j < lines; j++) {
                this._board[i][j] = undefined;
            }
        }


    }


    _is_placement_coords(position) {

        if ((position.x === 0 && position.y !== 0 && position.y !== 10)
            || ((position.x === 10) && (position.y !== 0) && (position.y !== 10))
            || (position.y === 0 && position.x !== 0 && position.x !== 10)
            || ((position.y === 10) && (position.x !== 0) && (position.x !== 10)))
            return true;

        else return false;

    }

    _fill_slot(piece) {

        if (this._board [piece.coordinates().x()] [piece.coordinates().y()] === undefined)
            this._board [piece.coordinates().x()] [piece.coordinates().y()] = piece;

        //else console.log("slot taken !");


    }

    _pop_piece(index_coords) {

        this._board [index_coords.x][index_coords.y] = undefined;


    }

    _slot_taken(position) {


        if (this._board [position.x] [position.y] !== undefined) {

            // console.log("taken");

            return true;
        } else {
            //console.log(" not taken");
            return false;
        }


    }


    _get_piece_At(position) {

        return this._board [position.x] [position.y];


    }

    _get_obstacles() {

        let obstacles = [];
        for (let i = 0; i < this.columns; i++) {
            for (let j = 0; j < this.lines; j++) {

                if (this._board[i][j] != undefined && this._board[i][j].color() === Color.RED) obstacles.push(this._board[i][j]);

            }
        }

        return obstacles;
    }

    _is_full() {


        for (let i = 0; i < this.columns; i++) {

            for (let j = 0; j < this.lines; j++) {
                let position = {
                    x: i,
                    y: j
                }
                if (!this._is_placement_coords(position)) continue;
                else if (this._board[i][j] === undefined) {
                    return false;
                }
            }

            return true;
        }


    }

    _unselect_anything() {

        for (let i = 0; i < this.columns; i++) {
            for (let j = 0; j < this.lines; j++) {

                if (this._board[i][j] != undefined && this._board[i][j].is_selected()) {

                    console.log(" THIS PIECE WAS SELECTED BEFORE", i, j, this._board[i][j].is_selected());
                    this._board[i][j].unselect_piece();
                    console.log("IS IT SELECTED NOW ?", i, j, this._board[i][j].is_selected());


                    return this._board[i][j];
                }

            }
        }

        return undefined;


    }

    _in_area (position,is_yellow) {

        if (position.x >= 1 && position.y >=1 && position.x<=3 && position.y<=3 ){
            if (is_yellow) this._yellow_score.area_0 += 1 ;
            else this._blue_score.area_0 +=1 ;

        }
        if (position.x >= 4 && position.y >=1 && position.x<=6 && position.y<=3 ){
            if (is_yellow) this._yellow_score.area_1 += 1 ;
            else this._blue_score.area_1 +=1 ;
        }
        if (position.x >= 7 && position.y >=1 && position.x<=9 && position.y<=3 ){
            if (is_yellow) this._yellow_score.area_2 += 1 ;
            else this._blue_score.area_2 +=1 ;

        }
        if (position.x >= 1 && position.y >=4 && position.x<=3 && position.y<=6 ) {
            if (is_yellow) this._yellow_score.area_3 += 1 ;
            else this._blue_score.area_3 +=1 ;
        }
        if (position.x >= 4 && position.y >=4 && position.x<=6 && position.y<=6 ) {
            if (is_yellow) this._yellow_score.area_4 += 1 ;
            else this._blue_score.area_4 +=1 ;
        }
        if (position.x >= 7 && position.y >=6 && position.x<=9 && position.y<=6 ) {
            if (is_yellow) this._yellow_score.area_5 += 1 ;
            else this._blue_score.area_5 +=1 ;
        }
        if (position.x >= 1 && position.y >=7 && position.x<=3 && position.y<=9 ) {
            if (is_yellow) this._yellow_score.area_6 += 1 ;
            else this._blue_score.area_6 +=1 ;
        }
        if (position.x >= 4 && position.y >=7 && position.x<=6 && position.y<=9 ) {

            if (is_yellow) this._yellow_score.area_7 += 1 ;
            else this._blue_score.area_7 +=1 ;
        }
        if (position.x >= 7 && position.y >=7 && position.x<=9 && position.y<=9 ) {
            if (is_yellow) this._yellow_score.area_8 += 1 ;
            else this._blue_score.area_8 +=1 ;
        }
    }



    check_dominant () {

        // 6 territories

        for (let i = 0; i < this.columns; i++) {
            for (let j = 0; j < this.lines; j++) {

                let piece_coords = {
                    x:undefined ,
                    y:undefined
                }

                if ( this._board[i][j] !== undefined) {

                     piece_coords = {
                        x: this._board[i][j].coordinates().x(),
                        y: this._board[i][j].coordinates().y()

                    }
                }

                if ( this._board[i][j] !== undefined && !this._is_placement_coords(piece_coords)) {

                    console.log("checking winner in progress .. ");


                    if (this._board[i][j].color() === 1) {

                        this._in_area(piece_coords, true);

                    } else if (this._board[i][j].color() === 2) {

                        this._in_area(piece_coords, false);

                    }
                }


            }
        }

        console.log("SCOOORE !! : " , this._blue_score) ;

      //  console.log("SCORE !!!!!!!!!!!!!!!!!!! ù%%%%",this._blue_score);
    }










    check_finish(current_color) {



        for (let i = 0; i < this.columns; i++) {
            for (let j = 0; j < this.lines; j++) {

              //  if (this._board[i][j] !== undefined && (this._board[i][j].color() !== Color.RED) && this._board[i][j].is_played()) {

                let checking_piece_coords = undefined ;
                let next_piece_coords = undefined ;



                let next_piece = undefined;

                if (this._board[i][j] !== undefined && this._board[i][j].color()=== current_color) {



                     checking_piece_coords =  {

                        x : this._board[i][j].coordinates().x() ,
                        y : this._board[i][j].coordinates().y()
                    }



                    // to bot

                    if (checking_piece_coords.y === 0 ) {

                        next_piece_coords = {

                            x : this._board[i][j].coordinates().x() ,
                            y : this._board[i][j].coordinates().y()+1
                        }

                        next_piece = this._get_piece_At(next_piece_coords) ;

                        if (next_piece === undefined) return false ;


                    }

                    // to top
                    else if (checking_piece_coords.y === 10) {

                        next_piece_coords = {

                            x : this._board[i][j].coordinates().x() ,
                            y : this._board[i][j].coordinates().y()-1
                        }

                        if (next_piece === undefined) return false ;

                    }

                    // to left
                    else if (checking_piece_coords.x === 0 ) {

                        next_piece_coords = {

                            x : this._board[i][j].coordinates().x()+1 ,
                            y : this._board[i][j].coordinates().y()
                        }

                        if (next_piece === undefined) return false ;


                    }

                    // to right
                    else if (checking_piece_coords.x === 10 ) {

                        next_piece_coords = {

                            x : this._board[i][j].coordinates().x()+1 ,
                            y : this._board[i][j].coordinates().y()
                        }

                        if (next_piece === undefined) return false ;
                    }
                }
            }

        }

        if (current_color === Color.BLUE) {
            this._blue_finished = true ;
        }

        else if (current_color === Color.YELLOW) {
            this._yellow_finished = true ;
        }
        return true ;


    }


}


export default Board;