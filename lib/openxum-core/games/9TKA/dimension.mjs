"use strict";

let Dimension = {DEFAULT_COLUMNS: 11, DEFAULT_LINES: 11};

export default Dimension;