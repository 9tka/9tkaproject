"use strict";

let MoveType = {PUT_PIECE: 0, MOVE_PIECE: 1};

export default MoveType;