"use strict";

class Coordinates {

    constructor(c, l) {
        this._column = c;
        this._line = l;
    }


    isValid() {

        return this._column >= 1 && this._column <=9 && this._line >= 1 && this._line <=9;
    }



    clone() {
        return new Coordinates(this._column, this._line);
    };


    x() {
        return this._column;
    };



    y() {
        return this._line;
    };



    hash() {

        return this._column.charCodeAt(0) + this._line * (4 * this._line) - 7;
    };

    equals(coordinate) {
        return this._column === coordinate.x() &&
            this._line === coordinate.y();
    };

    to_string() {
        if (!this.isValid()) {
            return "invalid";
        }
        return this._column + "" + this._line;
    }

    formatted_string() {
        return String.fromCharCode('A'.charCodeAt(0) + this.x()) + (this.y() + 1);
    }
}

export default Coordinates;