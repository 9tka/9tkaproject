"use strict";

//import Coordinates from './coordinates.mjs';
import OpenXum from '../../openxum/index.mjs';
import MoveType from '../9TKA/move_type.mjs';

class Move extends OpenXum.Move {
    constructor(position_from, position_to) {
        super();
        this._from = position_from;
        this._to = position_to;
    }

// public methods

    decode(str) {

        /*
        this._from = new init_Coordinates(parseInt(str.charAt(0)), parseInt(str.charAt(1)));
        this._to = new Coordinates(parseInt(str.charAt(2)), parseInt(str.charAt(3)));

<<<<<<< HEAD
        if (str[0] === 'M') {
            this._type = MoveType.MOVE_PIECE;

            this._to = {

            }
        } else {
            this._type = MoveType.PUT_PIECE;

            this._to = {

                //x :

            }
        }
=======
>>>>>>> origin/moteur
*/


    }

    /*
    Valide_Position(){
            return (this._from.x() == this._to.x() || this._from.y()== this._to.y());
    }
    Existe_Case(){

        if (this.Valide_Position()&&this.tab[this._to.x()][this._to.y()] == null) return true;
        else return false;
    }
*/
    from() {
        return this._from;
    }

    /*
    encode() {

        return this._from.to_string() + this._to.to_string();

    }*/

    encode() {

        if (this._type === MoveType.PUT_PIECE) {
            return "P" + this._color === Color.YELLOW ? 'Y' : 'B' + this._to.x + this._to.y;
        } else {
            return "M" + this._color === Color.RED ? 'Y' : 'B' + this._from.x + this._from.y + this._to.x + this._to.y;
        }

    }


        /*
to() {
    return this._to;
>>>>>>> origin/moteur
} */


        from_object(data)
        {
            this._from = new init_Coordinates(data.from.column, data.from.line);
            this._to = new Coordinates(data.to.column, data.to.line);
        }

        to_object()
        {
            return {
                from: this._from === null ? {column: -1, line: -1} : {
                    column: this._from._column,
                    line: this._from._line
                },
                to: this._to === null ? {column: -1, line: -1} : {column: this._to._column, line: this._to._line},
            };
        }


        to_string()
        {
            return 'move piece ' + this.from().to_string() + ' to ' + this.to().to_string();
        }

        formatted_string()
        {
            return 'move piece ' + this.from().formatted_string() + ' to ' + this.to().formatted_string();
        }

    }



export default Move;
