"use strict";

import Color from './color.mjs';
import Direction from './direction.mjs';
import Coordinates from './coordinates.mjs';
import GameType from './game_type.mjs';
import Intersection from './intersection.mjs';
import Move from './move.mjs';
import MoveType from './move_type.mjs';
import OpenXum from '../../openxum/index.mjs';
import Phase from './phase.mjs';
import PieceType from './piece_type.mjs';
import State from './state.mjs';

// grid constants definition
const begin_letter = ['A', 'A', 'A', 'A', 'A', 'B', 'C', 'D', 'E'];
const end_letter = ['E', 'F', 'G', 'H', 'I', 'I', 'I', 'I', 'I'];
const begin_number = [1, 1, 1, 1, 1, 2, 3, 4, 5];
const end_number = [5, 6, 7, 8, 9, 9, 9, 9, 9];
const begin_diagonal_letter = ['A', 'A', 'A', 'A', 'A', 'B', 'C', 'D', 'E'];
const end_diagonal_letter = ['E', 'F', 'G', 'H', 'I', 'I', 'I', 'I', 'I'];
const begin_diagonal_number = [5, 4, 3, 2, 1, 1, 1, 1, 1];
const end_diagonal_number = [9, 9, 9, 9, 9, 8, 7, 6, 5];

// initial state
const initial_type = [
  // column A
  PieceType.TOTT, PieceType.TOTT, PieceType.TOTT, PieceType.TOTT, PieceType.TOTT,
  // column B
  PieceType.TOTT, PieceType.TZARRA, PieceType.TZARRA, PieceType.TZARRA, PieceType.TZARRA, PieceType.TOTT,
  // column C
  PieceType.TOTT, PieceType.TZARRA, PieceType.TZAAR, PieceType.TZAAR, PieceType.TZAAR, PieceType.TZARRA, PieceType.TOTT,
  // column D
  PieceType.TOTT, PieceType.TZARRA, PieceType.TZAAR, PieceType.TOTT, PieceType.TOTT, PieceType.TZAAR, PieceType.TZARRA, PieceType.TOTT,
  // column E
  PieceType.TOTT, PieceType.TZARRA, PieceType.TZAAR, PieceType.TOTT, PieceType.TOTT, PieceType.TZAAR, PieceType.TZARRA, PieceType.TOTT,
  // column F
  PieceType.TOTT, PieceType.TZARRA, PieceType.TZAAR, PieceType.TOTT, PieceType.TOTT, PieceType.TZAAR, PieceType.TZARRA, PieceType.TOTT,
  // column G
  PieceType.TOTT, PieceType.TZARRA, PieceType.TZAAR, PieceType.TZAAR, PieceType.TZAAR, PieceType.TZARRA, PieceType.TOTT,
  // column H
  PieceType.TOTT, PieceType.TZARRA, PieceType.TZARRA, PieceType.TZARRA, PieceType.TZARRA, PieceType.TOTT,
  // column I
  PieceType.TOTT, PieceType.TOTT, PieceType.TOTT, PieceType.TOTT, PieceType.TOTT
];

const initial_color = [
  // column A
  Color.BLACK, Color.BLACK, Color.BLACK, Color.BLACK, Color.WHITE,
  // column B
  Color.WHITE, Color.BLACK, Color.BLACK, Color.BLACK, Color.WHITE, Color.WHITE,
  // column C
  Color.WHITE, Color.WHITE, Color.BLACK, Color.BLACK, Color.WHITE, Color.WHITE, Color.WHITE,
  // column D
  Color.WHITE, Color.WHITE, Color.WHITE, Color.BLACK, Color.WHITE, Color.WHITE, Color.WHITE, Color.WHITE,
  // column E
  Color.WHITE, Color.WHITE, Color.WHITE, Color.WHITE, Color.BLACK, Color.BLACK, Color.BLACK, Color.BLACK,
  // column F
  Color.BLACK, Color.BLACK, Color.BLACK, Color.BLACK, Color.WHITE, Color.BLACK, Color.BLACK, Color.BLACK,
  // column G
  Color.BLACK, Color.BLACK, Color.BLACK, Color.WHITE, Color.WHITE, Color.BLACK, Color.BLACK,
  // column H
  Color.BLACK, Color.BLACK, Color.WHITE, Color.WHITE, Color.WHITE, Color.BLACK,
  // column I
  Color.BLACK, Color.WHITE, Color.WHITE, Color.WHITE, Color.WHITE
];

// enums definition
const letters = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I'];

class Engine extends OpenXum.Engine {
  constructor(t, c) {
    super();
    this._type = t;
    this._current_color = c;

    this._placedTzaarPieceNumber = 0;
    this._placedPieceNumber = 0;
    this._phase = Phase.FIRST_MOVE;
    this._intersections = {};

    let k = 0;

    for (let i = 0; i < letters.length; ++i) {
      const l = letters[i];

      for (let n = begin_number[l.charCodeAt(0) - 'A'.charCodeAt(0)];
           n <= end_number[l.charCodeAt(0) - 'A'.charCodeAt(0)]; ++n) {
        if (l !== 'E' || n !== 5) {
          const coordinates = new Coordinates(l, n);

          this._intersections[coordinates.hash()] = new Intersection(coordinates, initial_color[k], initial_type[k]);
          ++k;
        }
      }
    }
  }

// public methods
  build_move() {
    return new Move();
  }

  can_capture(color) {
    let found = false;
    const list = this.get_no_free_intersections(color);
    let index = 0;

    while (!found && index < list.length) {
      found = this.get_distant_neighbors(list[index].coordinates(), color === Color.BLACK ? Color.WHITE : Color.BLACK, true).length > 0;
      ++index;
    }
    return found;
  }

  can_make_stack(color) {
    let found = false;
    const list = this.get_no_free_intersections(color);
    let index = 0;

    while (!found && index < list.length) {
      found = this.get_distant_neighbors(list[index].coordinates(), color, false).length > 0;
      ++index;
    }
    return found;
  }

  capture(origin, destination) {
    const origin_it = this._intersections[origin.hash()];
    const destination_it = this._intersections[destination.hash()];

    origin_it.capture(destination_it);
    if (this._phase === Phase.CAPTURE) {
      this._phase = Phase.CHOOSE;
    } else if (this._phase === Phase.SECOND_CAPTURE) {
      this._phase = Phase.CAPTURE;
      this._change_color();
    }
  }

  choose(capture, make_stack, pass) {
    if (capture) {
      this._phase = Phase.SECOND_CAPTURE;
    } else if (make_stack) {
      this._phase = Phase.MAKE_STRONGER;
    } else if (pass) {
      this.pass();
    }
  }

  clone() {
    let o = new Engine(this._type, this._current_color);

    o._set(this._phase, this._state, this._intersections, this._placedTzaarPieceNumber, this._placedPieceNumber);
    return o;
  }

  current_color() {
    return this._current_color;
  }

  exist_intersection(letter, number) {
    const coordinates = new Coordinates(letter, number);

    if (coordinates.is_valid()) {
      return this._intersections[coordinates.hash()] !== null;
    } else {
      return false;
    }
  }

  first_move(origin, destination) {
    const origin_it = this._intersections[origin.hash()];
    const destination_it = this._intersections[destination.hash()];

    origin_it.capture(destination_it);
    this._phase = Phase.CAPTURE;
    this._change_color();
  }

  get_distant_neighbors(origin, color, control) {
    let list = [];
    const origin_it = this._intersections[origin.hash()];
    let direction = Direction.NORTH_WEST;
    let stop = false;

    while (!stop) {
      let distance = 0;
      let destination;
      let destination_it;

      do {
        ++distance;
        destination = origin.move(distance, direction);
        if (destination.is_valid()) {
          destination_it = this._intersections[destination.hash()];
        }
      } while (destination.is_valid() && destination_it.state() === State.VACANT);
      if (destination.is_valid() && destination_it.state() === State.NO_VACANT && destination_it.color() === color) {
        if (!control || (control && origin_it.size() >= destination_it.size())) {
          list.push(destination);
        }
      }
      if (direction === Direction.SOUTH_WEST) {
        stop = true;
      } else {
        direction = this._next_direction(direction);
      }
    }
    return list;
  }

  get_free_intersections() {
    let list = [];

    for (let index in this._intersections) {
      let intersection = this._intersections[index];

      if (intersection.state() === State.VACANT) {
        list.push(intersection.coordinates());
      }
    }
    return list;
  }

  get_intersection(letter, number) {
    return this._intersections[(new Coordinates(letter, number)).hash()];
  }

  get_intersections() {
    return this._intersections;
  }

  get_name() {
    return 'Tzaar';
  }

  get_no_free_intersections(color) {
    let list = [];

    for (let index in this._intersections) {
      const intersection = this._intersections[index];

      if (intersection.state() === State.NO_VACANT && intersection.color() === color) {
        list.push(intersection);
      }
    }
    return list;
  }

  get_possible_capture(coordinates) {
    return this.get_distant_neighbors(coordinates, this._intersections[coordinates.hash()].color() === Color.BLACK ? Color.WHITE : Color.BLACK, true);
  }

  get_possible_make_stack(coordinates) {
    return this.get_distant_neighbors(coordinates, this._intersections[coordinates.hash()].color(), false);
  }

  get_possible_move_list() {
    let list = [];

    if (this._phase === Phase.FIRST_MOVE) {
      const intersections = this.get_no_free_intersections(this._current_color);

      intersections.forEach((e) => {
        const neighbors = this.get_distant_neighbors(e.coordinates(), this._current_color, false);

        neighbors.forEach((n) => {
          list.push(new Move(MoveType.FIRST_MOVE, this._current_color, e.coordinates(), n));
        });
      });
    } else if (this._phase === Phase.CAPTURE) {
      const intersections = this.get_no_free_intersections(this._current_color);

      intersections.forEach((e) => {
        const neighbors = this.get_possible_capture(e.coordinates());

        neighbors.forEach((n) => {
          list.push(new Move(MoveType.CAPTURE, this._current_color, e.coordinates(), n));
        });
      });
    } else if (this._phase === Phase.CHOOSE) {
      const intersections = this.get_no_free_intersections(this._current_color);
      let ok = true;

      intersections.forEach((e) => {
        if (this.get_possible_capture(e.coordinates()).length > 0) {
          ok = false;
          return;
        }
      });
      if (!ok) {
        list.push(new Move(MoveType.CHOOSE, this._current_color, null, null, Phase.SECOND_CAPTURE));
      }
      ok = true;
      intersections.forEach((e) => {
        if (this.get_possible_make_stack(e.coordinates()).length > 0) {
          ok = false;
          return;
        }
      });
      if (!ok) {
        list.push(new Move(MoveType.CHOOSE, this._current_color, null, null, Phase.MAKE_STRONGER));
      }
      list.push(new Move(MoveType.CHOOSE, this._current_color, null, null, Phase.PASS));
    } else if (this._phase === Phase.SECOND_CAPTURE) {
      const intersections = this.get_no_free_intersections(this._current_color);

      intersections.forEach((e) => {
        const neighbors = this.get_possible_capture(e.coordinates());

        neighbors.forEach((n) => {
          list.push(new Move(MoveType.SECOND_CAPTURE, this._current_color, e.coordinates(), n));
        });
      });
    } else if (this._phase === Phase.MAKE_STRONGER) {
      const intersections = this.get_no_free_intersections(this._current_color);

      intersections.forEach((e) => {
        const neighbors = this.get_possible_make_stack(e.coordinates());

        neighbors.forEach((n) => {
          list.push(new Move(MoveType.MAKE_STRONGER, this._current_color, e.coordinates(), n));
        });
      });
    }

    if (list.length === 0) {
      console.log("ERROR", this._phase);
    }

    return list;
  }

  get_state() {
    return this._state;
  }

  get_type() {
    return this._type;
  }

  is_finished() {
    return (!this.can_capture(Color.BLACK) || !this._is_all_types_presented(Color.BLACK) || 
      !this.can_capture(Color.WHITE) || !this._is_all_types_presented(Color.WHITE));
  }

  make_stack(origin, destination) {
    const origin_it = this._intersections[origin.hash()];
    const destination_it = this._intersections[destination.hash()];

    origin_it.move_stack_to(destination_it);
    this._phase = Phase.CAPTURE;
    this._change_color();
  }

  move(move) {
    if (move.type() === MoveType.FIRST_MOVE) {
      this.first_move(move.from(), move.to());
    } else if (move.type() === MoveType.CAPTURE) {
      this.capture(move.from(), move.to());
    } else if (move.type() === MoveType.CHOOSE) {
      this.choose(move.choice() === Phase.SECOND_CAPTURE,
        move.choice() === Phase.MAKE_STRONGER,
        move.choice() === Phase.PASS);
    } else if (move.type() === MoveType.SECOND_CAPTURE) {
      this.capture(move.from(), move.to());
    } else if (move.type() === MoveType.MAKE_STRONGER) {
      this.make_stack(move.from(), move.to());
    }
  }

  move_stack(origin, destination) {
    const origin_it = this._intersections[origin.hash()];
    const destination_it = this._intersections[destination.hash()];

    origin_it.move_stack_to(destination_it);
    this._change_color();
  }

  parse(str) {
    // TODO
  }

  pass() {
    this._phase = Phase.CAPTURE;
    this._change_color();
  }

  phase() {
    return this._phase;
  }

  to_string() {
    // TODO
  }

  verify_capture(origin, destination) {
    const origin_it = this._intersections[origin.hash()];
    const list = this.get_distant_neighbors(origin, origin_it.color() === Color.BLACK ? Color.WHITE : Color.BLACK, true);
    let found = false;

    for (let index in list) {
      if (list[index].hash() === destination.hash()) {
        found = true;
        break;
      }
    }
    return found;
  }

  winner_is() {
    if (this.is_finished()) {
      return this._current_color;
    } else {
      return false;
    }
  }

// private methods
  _change_color() {
    this._current_color = this._current_color === Color.BLACK ? Color.WHITE : Color.BLACK;
  }

  _next_direction(direction) {
    if (direction === Direction.NORTH_WEST) {
      return Direction.NORTH;
    } else if (direction === Direction.NORTH) {
      return Direction.NORTH_EAST;
    } else if (direction === Direction.NORTH_EAST) {
      return Direction.SOUTH_EAST;
    } else if (direction === Direction.SOUTH_EAST) {
      return Direction.SOUTH;
    } else if (direction === Direction.SOUTH) {
      return Direction.SOUTH_WEST;
    } else if (direction === Direction.SOUTH_WEST) {
      return Direction.NORTH_WEST;
    }
  }

  _is_all_types_presented(color) {
    let tzaar_found = false;
    let tzara_found = false;
    let tott_found = false;

    for (let index in this._intersections) {
      const intersection = this._intersections[index];

      if (intersection.state() === State.NO_VACANT && intersection.color() === color) {
        if (intersection.type() === PieceType.TZAAR) {
          tzaar_found = true;
        }
        if (intersection.type() === PieceType.TZARRA) {
          tzara_found = true;
        }
        if (intersection.type() === PieceType.TOTT) {
          tott_found = true;
        }
      }
      if (tzaar_found && tzara_found && tott_found) {
        break;
      }
    }
    return tzaar_found && tzara_found && tott_found;
  }
}

export default Engine;